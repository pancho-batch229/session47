// console.log("hello");

// DISCUSSION #1

const textFirstName = document.querySelector("#text-first-name");
const textLastName = document.querySelector("#text-last-name");
const spanFullName = document.querySelector("#span-full-name");

// using "keyup" event
/*
textFirstName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = textFirstName.value;
})

textFirstName.addEventListener("keyup", (event) => {
	console.log(event.target);
	console.log(event.target.value);
})
*/

// DISCUSSION #2
const updateFullName = () => {
	let firstName = textFirstName.value;
	let lastName = textLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`;
}

textFirstName.addEventListener("keyup", updateFullName);
textLastName.addEventListener("keyup", updateFullName);